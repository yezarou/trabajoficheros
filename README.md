# Trabajo sobre Ficheros.

## Cosas a tener en cuenta: 
- Reaprovecho la clase Timer de mi anterior ejercicio.
- La clase Conexión es igual a la clase RestClient + Conexion, unidas para que sea más sencillo.

## Funcionamiento del ejercicio.
El ejercicio es simple. Cuando insertamos dos URLs en los edittext y descargamos, lo primero que ocurre es ocultarse el teclado y comprobar si tenemos conexión a internet.
De no tenerlo, aparece un Toast indicándolo, y no ocurrirá nada más. A continuación se muestra un dialog de progreso y se descargan los ficheros con las URL de la imagen y las frases en memoria interna.
Si hay problema con los enlaces, se escribirá en un Log de error (explicado más adelante).  
Si todo funciona, se ejecuta un Timer cada X tiempo (indicado en el archivo que hay en la carpeta res/raw), y cada ese intervalo de segundos, se cambiará de una URL de la imagen, a otra. Igual con
las frases. (Necesario que sean más de 23 segundos aprox para que a Picasso le de tiempo a detectar que la url de una imagen no funciona.)  
Cada vez que NO se muestra una imagen correctamente, se manda un mensaje de error.  
Cuando el timer finaliza, vuelve a ejecutarse a si mismo con el mismo intervalo para que vuelvan a ir rotando las imagenes/frases.  

Ahora, para subir el archivo de errores a la red lo que hago es leer y guardar el archivo que hay situado en http://ruben.alumno.mobi/uploads/errores.txt. Una vez descargado, añadimos una linea del
error con la fecha actual (clase Date + SimpleTimeFormat), y, una vez guardado este archivo, utilizaremos un php de subida de archivos que permite que un archivo se pueda sobreescribir para subirse a la red.

## Mejoras para el proyecto.
El código necesita un refactorizado para que funcione con mejor rendimiento, hacer que el error de Picasso salte antes, mejoras visuales y guardar uploadError.php en otro sitio para que no se pueda acceder fácilmente al archivo.