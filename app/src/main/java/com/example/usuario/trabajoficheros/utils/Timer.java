package com.example.usuario.trabajoficheros.utils;

import android.os.CountDownTimer;

public class Timer extends CountDownTimer {
    OnTickListener listener;
    int minutes;
    int seconds;

    public interface OnTickListener {
        void onTick();
        void onFinish();
    }

    public Timer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    @Override
    public void onTick(long millisUntilFinished) {
        minutes = (int) millisUntilFinished / 1000 / 60;
        seconds = (int) millisUntilFinished / 1000 % 60;

        listener.onTick();
    }

    @Override
    public void onFinish() {
        listener.onFinish();
    }

    @Override
    public String toString() {
        return format(minutes) + ":" + format(seconds);
    }

    private String format(int valor) {
        return String.format("%02d", valor);
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setOnTickListener(OnTickListener listener) {
        this.listener = listener;
    }
}

