package com.example.usuario.trabajoficheros.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.trabajoficheros.R;
import com.example.usuario.trabajoficheros.network.Conexion;
import com.example.usuario.trabajoficheros.pojo.Resultado;
import com.example.usuario.trabajoficheros.utils.Keyboard;
import com.example.usuario.trabajoficheros.utils.Memoria;
import com.example.usuario.trabajoficheros.utils.Timer;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import okhttp3.OkHttpClient;

public class VisorActivity extends AppCompatActivity {

    private static final String IMAGEFILE = "images.txt";
    private static final String SENTENCEFILE = "sentences.txt";
    private static final String ERRORFILE = "errores.txt";
    private static final String CODIFICACION = "UTF-8";
    private static final String SEGUNDOS = "intervalo";
    private static final String UPLOADFILE = "http://ruben.alumno.mobi/errorUpload.php";
    private static final String ERRORURL = "http://ruben.alumno.mobi/uploads/errores.txt";
    public final static String PASS = "hola";

    private EditText edImageUrl, edSentenceUrl;
    private TextView tvSentence, tvImageSeconds, tvSentenceSeconds;
    private ImageView ivImage;
    private Button btDownload;

    private String[] images;
    private int imagePos;

    private String[] sentences;
    private int sentencePos;

    private ProgressDialog progreso;
    private Timer timer;

    private long iniTime;
    private long imageTime;
    private long sentenceTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visor);

        initialice();
    }

    private void initialice(){
        edImageUrl = findViewById(R.id.edImageUrl);
        edSentenceUrl = findViewById(R.id.edSentenceUrl);
        ivImage = findViewById(R.id.ivImage);
        tvSentence = findViewById(R.id.tvSentence);
        btDownload = findViewById(R.id.btDownload);
        tvImageSeconds = findViewById(R.id.tvImageSeconds);
        tvSentenceSeconds = findViewById(R.id.tvSentenceSeconds);

        btDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFiles();
            }
        });

        progreso = new ProgressDialog(this);
        initialiceProgressDialog();
    }

    private void initialiceProgressDialog(){
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Conectando . . .");
        progreso.setCancelable(true);
        progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Conexion.cancelRequests(getApplicationContext(), true);
            }
        });
    }

    private void initialiceTimer(final long intervalo){
        if (timer != null)
            timer.cancel();
        timer = new Timer(intervalo, intervalo);
        timer.setOnTickListener(new Timer.OnTickListener() {
            @Override
            public void onTick() { }

            @Override
            public void onFinish() {
                initialiceTimer(intervalo);
                timer.start();
                changePos();
            }
        });
    }

    private void changePos() {
        try {
            imagePos = (imagePos + 1) % images.length;
            downloadImage();
        } catch (Exception e) { }
        try {
            sentencePos = (sentencePos + 1) % sentences.length;
            showSentence();
        } catch (Exception e) { }
    }

    private void downloadFiles(){
        Keyboard.hideSoftKeyboard(this);
        if (Conexion.isNetworkAvailable(this)) {
            try {
                long intervalo = readSecondsRaw();
                iniTime = System.currentTimeMillis();
                progreso.show();
                downloadImageFile();
                downloadSentenceFile();
                initialiceTimer(intervalo);
                timer.start();
            } catch (NumberFormatException e) {
                error(e.getMessage());
            } catch (Exception e) {
                error(e.getMessage());
            }

        } else
            Toast.makeText(this, "No hay conexión a internet. ", Toast.LENGTH_SHORT).show();
    }

    //region Imagenes
    private void downloadImageFile(){
        File miFichero = new File(getFilesDir() + "/" + IMAGEFILE);
        Conexion.get(edImageUrl.getText().toString(), new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                error("Fallo: " + statusCode + " " + throwable.getMessage() + edImageUrl.getText().toString());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                imagePos = 0;
                imageTime = System.currentTimeMillis();
                tvImageSeconds.setText((imageTime - iniTime) + " ms Image File");
                readImageFile();
                downloadImage();
            }
        });
    }

    private void readImageFile(){
        Resultado resultado = new Memoria(this).leerInterna(IMAGEFILE, CODIFICACION);
        images = resultado.getContenido().split("\n");
    }

    public void downloadImage(){
        final String url = images[imagePos];

        OkHttpClient client = new OkHttpClient();
        Picasso picasso = new Picasso.Builder(this)
                .downloader(new OkHttp3Downloader(client))
                .build();

        picasso.get()
                .load(url)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder_error)
                .into(ivImage, new Callback() {
                    @Override
                    public void onSuccess() { }
                    @Override
                    public void onError(Exception e) {
                        error("Error en la descarga: " + url);
                    }
                });
    }

    //endregion

    //region Frases
    private void downloadSentenceFile(){
        File miFichero = new File(getFilesDir() + "/" + SENTENCEFILE);
        Conexion.get(edSentenceUrl.getText().toString(), new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                error( "Fallo: " + statusCode + " " + throwable.getMessage() + edSentenceUrl.getText().toString());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                sentencePos = 0;
                sentenceTime = System.currentTimeMillis();
                tvSentenceSeconds.setText((sentenceTime - iniTime) + " ms Text File");
                readSentenceFile();
                showSentence();
            }
        });
    }

    private void readSentenceFile(){
        Resultado resultado = new Memoria(this).leerInterna(SENTENCEFILE, CODIFICACION);
        sentences = resultado.getContenido().split("\n");
    }

    public void showSentence() {
        tvSentence.setText(sentences[sentencePos]);
    }
    //endregion

    private long readSecondsRaw() throws NumberFormatException, Exception {
        Resultado resultado = new Memoria(this).leerRaw(SEGUNDOS);
        if (!resultado.getCodigo())
            throw new Exception();
        return Long.parseLong(resultado.getContenido()) * 1000;
    }

    private void error(final String message){
        File miFichero = new File(getFilesDir() + "/" + ERRORFILE);
        Conexion.get(ERRORURL, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                addErrorLine(message, false);
                uploadErrorFile();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                addErrorLine(message, true);
                uploadErrorFile();
            }
        });
    }

    private void addErrorLine(String mensaje, boolean newFile){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
        String error = "[" + simpleDateFormat.format(new Date().getTime()) + "] " + mensaje + "\r\n";
        new Memoria(this).escribirInterna(ERRORFILE, error, newFile, CODIFICACION);
    }

    private void uploadErrorFile() {
        File myFile = new File(getFilesDir() + "/" + ERRORFILE);

        RequestParams params = new RequestParams();
        try {
            params.put("fileToUpload", myFile);
            params.put("password", PASS);
            Conexion.post(UPLOADFILE, params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toast.makeText(VisorActivity.this, "Fallo: " + statusCode + "\n" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Toast.makeText(VisorActivity.this, "Success: " + statusCode + "\n" + responseString, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (FileNotFoundException e) {
            Toast.makeText(this,"ERROR: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
